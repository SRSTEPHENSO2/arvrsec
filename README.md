# SoK: Authentication in Augmented and Virtual Reality

This repository contains supporting content for the paper SoK: Authentication in Augmented and Virtual Reality (*IEEE Symposium on Security and Privacy 2022*). 

In this work, we evaluate the state-of-the-art of authentication mechanisms for AR/VR devices by systematizing research efforts and practical deployments. 
By studying users' experiences with authentication on AR and VR, we gain insight into the important properties needed for authentication on these devices. We then use these properties to perform a comprehensive evaluation of AR/VR authentication mechanisms both proposed in literature and used in practice. In all, we synthesize a coherent picture of the current state of authentication mechanisms for AR/VR devices. 

The content of our user survey can be found in `survey_content.pdf`.
